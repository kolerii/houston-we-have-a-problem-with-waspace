<?php
	/*
		NAME    : Houston, we have a problem with WaspAce
		VERSION : 1.2
		AUTHOR  : kolerii ( http://kolerii.ru/ )
	*/
	define( '_EXEC' , true );

	define( '_path' , str_replace( 'run.php' , '' , $_SERVER['SCRIPT_FILENAME'] ));

	include( 'config.php' );
	include( 'sms.class.php' );

	function isDownAPI()
	{
		$ch = curl_init();

		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_TIMEOUT, 5);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_URL, url_api);

		$html = trim(curl_exec($ch));

		curl_close($ch);

		return $html != '<html>Nothing here</html>'; 
	}

	function ping($addr,$port)
	{

		if($fp = @fsockopen('5.79.64.236','6066',$errCode,$errStr,1))
		{   
			fclose($fp);
			return false;
		}
		else
		   return true;
	}
	function isDownSurf()
	{
		if(ping('5.79.64.236','6066') || ping('5.79.64.236','6064'))
			return true;
		return false;
	}

	function xmail( $text )
	{
		$email = user_email;
		if(!empty($email))
		{
			$bound="--unoz--";
			$headers="From: Swigert <swigert@unoz.ru>\n";
			$headers.="To: " . $email . "\n";
			$headers.="Subject: Houston we have a problem with WaspAce\n";
			$headers.="Mime-Version: 1.0\n";
			$headers.="Content-Type: multipart/alternative; boundary=\"$bound\"\n";
			$body="--$bound\n";
			$body.="Content-type: text/html; charset=\"utf-8\"\n";
			$body.="Content-Transfer-Encoding: 8bit\n\n";

			$body.="\n".$text;
			$body.="\n\n--$bound\n";
			return @mail($email, $subj, $body, $headers);
		}
	}

	function notice_all($text)
	{
		xmail($text);
		$phone = user_phone;
		if(!empty($phone))
		{
			$sms = new sms(api_id_smsru);
			$sms->send($phone,$text);
			$sms = null;
		}
	}

	$_status_api = isDownAPI();
	$_status_srv = isDownSurf();

	$_tmp    = json_decode( @file_get_contents( _path . 'tmp') , true );

	file_put_contents( _path . 'tmp', json_encode(array('api' => $_status_api ? 1 : 2 , 'srv' => $_status_srv ? 1 : 2)));

	if(!empty($_tmp['api']))
	{
		$_saved_status = $_tmp['api'] == 2 ? false : true;

		if( $_status_api != $_saved_status )
		{
			$message = $_status_api ? 'Сервер API недоступен!' : 'Сервер API снова доступен!';

			if( ( type_work_api == '0' || type_work_api == '2' ) && $_status_api && type_work_api != -1 ) notice_all($message);
			if( ( type_work_api == '1' || type_work_api == '2' ) && !$_status_api && type_work_api != -1 ) notice_all($message);
		}
	}

	if(!empty($_tmp['srv']))
	{
		$_saved_status = $_tmp['srv'] == 2 ? false : true;

		if( $_status_srv != $_saved_status )
		{
			$message = $_status_srv ? 'Сервер серфинга недоступен!' : 'Сервер серфинга снова доступен!';

			if( ( type_work_srv == '0' || type_work_srv == '2' ) && date('i') != '00' && $_status_srv && type_work_srv != -1  ) notice_all($message);
			if( ( type_work_srv == '1' || type_work_srv == '2' ) && date('i') != '01' && !$_status_srv && type_work_srv != -1 ) notice_all($message);
		}
	}
?>