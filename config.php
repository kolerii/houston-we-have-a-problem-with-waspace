<?php
	defined( '_EXEC') or die( 'Access denied!' );

	//Контактные данные
	define( 'user_phone' , 'YOUR_NUMBER_PHONE' );
	define( 'user_email' , 'YOUR_EMAIL' );

	/*
		Константы `type_work_api` и `type_work_srv` устанавливают, какие события должны отслеживаться по серверу API и серфинга соответственно.
		-> API / type_work_api:
			*Уведомлять о недоступности 								-> 0
			*Уведомлять о доступности сервера после его недоступности   -> 1
			*Уведомлять о всех случаях									-> 2
			*Уведомление выключено										-> -1

		-> SURFING / type_work_srv:
			*Уведомлять о недоступности 								-> 0
			*Уведомлять о доступности сервера после его недоступности   -> 1
			*Уведомлять о всех случаях									-> 2
			*Уведомление выключено										-> -1
	*/
			
	define( 'type_work_api' , 2 );
	define( 'type_work_srv' , 2 );

	define( 'url_api'   , 'http://api.waspace.net/' );
	define( 'api_id_smsru' , 'YOUR_ID');
?>
